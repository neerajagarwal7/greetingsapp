FROM openjdk
WORKDIR app
ADD target/springbootWeb-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-jar","springbootWeb-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
