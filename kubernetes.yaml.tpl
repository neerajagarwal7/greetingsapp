apiVersion: apps/v1
kind: Deployment
metadata:
  name: greetingsapp
  labels:
    app: greetingsapp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: greetingsapp
  template:
    metadata:
      labels:
        app: greetingsapp
    spec:
      containers:
      - name: greetingsapp
        image: gcr.io/GOOGLE_CLOUD_PROJECT/greetingsapp:COMMIT_SHA
        ports:
        - containerPort: 8080
---
kind: Service
apiVersion: v1
metadata:
  name: greetingsapp
spec:
  selector:
    app: greetingsapp
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
  type: LoadBalancer

